package fr.uavignon.recyclerviewdemo;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import data.Country;

import static data.Country.*;

public class DetailFragment extends Fragment {

    public static final String TAG = "SecondFragment";
    TextView textView;
    TextView textView1;
    TextView textView2;
    TextView textView3;
    TextView textView4;
    TextView textView5;

    ImageView img;

    private Country[]tabcountr=Country.countries;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = view.findViewById(R.id. textview_second) ;
        textView1 = view.findViewById(R.id.textView2);
        textView2 = view.findViewById(R.id.textView3);
        textView3 = view.findViewById(R.id.textView4);
        textView4 = view.findViewById(R.id.textView5);
        textView5 = view.findViewById(R.id.textView6);
        img= view.findViewById(R.id.drapeau);

        // Implementation with bundle

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        textView1.setText(""+ countries[args.getCountryId()].getName());
        textView.setText(""+ countries[args.getCountryId()].getCapital());
        textView2.setText(""+ countries[args.getCountryId()].getLanguage());
        textView3.setText(""+ countries[args.getCountryId()].getCurrency());
        textView4.setText(+ countries[args.getCountryId()].getArea());
        textView5.setText(+ countries[args.getCountryId()].getPopulation()+" Km2");

        String uri = tabcountr[args.getCountryId()].getImgUri();
        Context c = img.getContext();
        img.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri, null , c.getPackageName())));

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);

            }
        });
    }
}